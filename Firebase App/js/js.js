 // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyA7Y0yJsmvWIG0CYBsRR2atsUWTnW6ivkk",
    authDomain: "rogers-a724b.firebaseapp.com",
    projectId: "rogers-a724b",
    storageBucket: "rogers-a724b.appspot.com",
    messagingSenderId: "349910913061",
    appId: "1:349910913061:web:d5361d0bd8d266733d6fcc",
    measurementId: "G-1GFVJNX0CE"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  //BotonRegistrar
  var firebaseRegister=
    document.querySelector(".botons");

  //Correo
  var firebaseEmail=
  document.querySelector(".correo");
  //Contraseña
  var firebasePassword=
  document.querySelector(".contraseña");
  //Registrar
function reg (){
    firebase.auth().createUserWithEmailAndPassword(firebaseEmail.value, firebasePassword.value)
  .then((user) => {
    // Signed in
    // ...
  })
  .catch((error) => {
    var errorCode = error.code;
    var errorMessage = error.message;
    // ..
  });
}

//Logear con Google
function regGoogle(provider) {
  var provider = new firebase.auth.GoogleAuthProvider();
  // [START auth_google_signin_popup]
  firebase.auth()
    .signInWithPopup(provider)
    .then((result) => {
      /** @type {firebase.auth.OAuthCredential} */
      var credential = result.credential;

      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      // ...
    }).catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });
}
//Logear con Facebook
function regFacebook(provider) {
  var provider = new firebase.auth.FacebookAuthProvider();
  // [START auth_facebook_signin_popup]
  firebase
    .auth()
    .signInWithPopup(provider)
    .then((result) => {
      /** @type {firebase.auth.OAuthCredential} */
      var credential = result.credential;

      // The signed-in user info.
      var user = result.user;

      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      var accessToken = credential.accessToken;

      // ...
    })
    .catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;

      // ...
    });
}
