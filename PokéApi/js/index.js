let _main = document.querySelector('#contenedor1') 

_main.addEventListener('click', pokemon);


function pokemon(){
    fetch("http://pokeapi.co/api/v2/pokemon/pikachu")
    .then((resp)=>resp.json())
    .then((res)=>{
        _main.innerHTML=`
            <h2>${res.name}</h2>
            <img src='${res.sprites.front_shiny}' width='300px'></img>
            <div class="uno">
                <h3>STATS</h3>
                    <p><span>${res.stats[0].stat.name} : </span>
                    <span>${res.stats[0].base_stat}</span></p>
                    <p><span>${res.stats[1].stat.name} : </span>
                    <span>${res.stats[1].base_stat}</span></p>
                    <p><span>${res.stats[2].stat.name} : </span>
                    <span>${res.stats[2].base_stat}</span></p>
                    <p><span>${res.stats[3].stat.name} : </span>
                    <span>${res.stats[3].base_stat}</span></p>
                    <p><span>${res.stats[4].stat.name} : </span>
                    <span>${res.stats[4].base_stat}</span></p>
                    <p><span>${res.stats[5].stat.name} : </span>
                    <span>${res.stats[5].base_stat}</span></p>
                <h3>MOVES</h3>
                    <p><span>${res.moves[22].move.name}: learned at lvl </span>
                    <span>${res.moves[22].version_group_details[0].level_learned_at}</span></p>
                    <span>${res.moves[10].move.name}: learned at lvl </span>
                    <span>${res.moves[10].version_group_details[0].level_learned_at}</span>
                    <p><span>${res.moves[9].move.name}: learned at lvl </span>
                    <span>${res.moves[9].version_group_details[0].level_learned_at}</span></p>
                    <span>${res.moves[3].move.name}: learned at lvl </span>
                    <span>${res.moves[3].version_group_details[0].level_learned_at}</span>
                    <p><span>${res.moves[22].move.name}: learned at lvl </span>
                    <span>${res.moves[22].version_group_details[0].level_learned_at}</span></p>
                <h3>TYPE</h3>
                    <span>${res.types[0].type.name}</span>
            </div>
        `;
        console.log(res);
    })
}
let _main2 = document.querySelector('#contenedor2') 

_main2.addEventListener('click', _pokemon);


function _pokemon(){
    fetch("https://pokeapi.co/api/v2/pokemon/raichu")
    .then((_resp)=>_resp.json())
    .then((_res)=>{
        _main2.innerHTML=`
            <h2>${_res.name}</h2>
            <img src='${_res.sprites.front_shiny}' width='300px'></img>
            <div class="uno">
                <h3>STATS</h3>
                    <p><span>${_res.stats[0].stat.name} : </span>
                    <span>${_res.stats[0].base_stat}</span></p>
                    <p><span>${_res.stats[1].stat.name} : </span>
                    <span>${_res.stats[1].base_stat}</span></p>
                    <p><span>${_res.stats[2].stat.name} : </span>
                    <span>${_res.stats[2].base_stat}</span></p>
                    <p><span>${_res.stats[3].stat.name} : </span>
                    <span>${_res.stats[3].base_stat}</span></p>
                    <p><span>${_res.stats[4].stat.name} : </span>
                    <span>${_res.stats[4].base_stat}</span></p>
                    <p><span>${_res.stats[5].stat.name} : </span>
                    <span>${_res.stats[5].base_stat}</span></p>
                <h3>MOVES</h3>
                    <p><span>${_res.moves[22].move.name}: learned at lvl </span>
                    <span>${_res.moves[22].version_group_details[0].level_learned_at}</span></p>
                    <span>${_res.moves[10].move.name}: learned at lvl </span>
                    <span>${_res.moves[10].version_group_details[0].level_learned_at}</span>
                    <p><span>${_res.moves[9].move.name}: learned at lvl </span>
                    <span>${_res.moves[9].version_group_details[0].level_learned_at}</span></p>
                    <span>${_res.moves[3].move.name}: learned at lvl </span>
                    <span>${_res.moves[3].version_group_details[0].level_learned_at}</span>
                    <p><span>${_res.moves[22].move.name}: learned at lvl </span>
                    <span>${_res.moves[22].version_group_details[0].level_learned_at}</span></p>
                <h3>TYPE</h3>
                    <span>${_res.types[0].type.name}</span>
            </div>
        `;
        console.log(_res);
    })
}
